//
//  ViewController.swift
//  LocalNotifications
//
//  Created by Sebas on 10/1/18.
//  Copyright © 2018 Sebas. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController {

    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var outputTextField: UILabel!
    
    var notificationMessage = "El texto es: "
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Para recuperar
        //1. Instanciar UserDefaults
        let defaults = UserDefaults.standard
        
        //2. Acceder al valor por medio del KEY
        outputTextField.text = defaults.object(forKey: "label") as? String
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert]) { (granted, error) in
            
        }
        
        
    }
    @IBAction func saveButtonPressed(_ sender: Any) {
        outputTextField.text = inputTextField.text
        
        // Para guardar
        //1. Instanciar UserDefaults
        let defaults = UserDefaults.standard
        
        //2. Guardar variables en defaults
        // Int, Bool, Float, Double, Object!
        
        defaults.set(inputTextField.text, forKey: "label")
        
        sendNotification()
        
    }
    
    func sendNotification() {
        notificationMessage += outputTextField.text!
        
        // 1. Authorization request ( está en DidLoad)
        // 2. Crear contenido de la notification
        
        let content = UNMutableNotificationContent()
        content.title = "Notification Title"
        content.subtitle = "Notification Subtitle"
        content.body = notificationMessage
        
        // 3. Definir un trigger
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 15, repeats: false)
        
        // 4. Definir un identifier para la notification
        
        let identifier = "Notification"
        
        // 5. Crear un Request
        
        let notificationRequest = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        // 6. Añadir el request al UNUserNotificationCenter
        
        UNUserNotificationCenter.current().add(notificationRequest) { (error) in
            
        }
        
    }
    
}







